ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/opensource/python/python39
ARG BASE_TAG=v3.9.7

FROM koalaman/shellcheck:v0.8.0 as source

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

USER 0

WORKDIR /mnt

COPY --from=source /bin/shellcheck /bin/

RUN dnf upgrade -y && \
dnf install -y jq && \
    dnf clean all && \
    rm -rf /var/cache/dnf && \
    groupadd -g 1002 shellcheck && \
    useradd -r -u 1002 -m -s /sbin/nologin -g shellcheck shellcheck && \
    chmod 755 /bin/shellcheck && \
    chown root:root /bin/shellcheck

COPY *.tar.gz *.whl /tmp/

WORKDIR /tmp/

RUN pip install --no-deps  PyYAML-6.0-cp39-cp39-manylinux_2_5_x86_64.manylinux1_x86_64.manylinux_2_12_x86_64.manylinux2010_x86_64.whl argcomplete.tar.gz setuptools-59.4.0-py3-none-any.whl xmltodict.tar.gz toml.tar.gz yq.tar.gz && \
    rm *.tar.gz *.whl

USER shellcheck

HEALTHCHECK NONE

ENTRYPOINT ["/bin/shellcheck"]
